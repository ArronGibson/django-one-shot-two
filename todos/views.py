from django.shortcuts import render, get_object_or_404
from .models import TodoList, TodoItem


# Create your views here.
def todo_list_list(request):
    model_list = TodoList.objects.all()
    context = {
        "model_list": model_list
    }
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": todolist
    }
    return render(request, "todos/todo_list_detail.html", context)